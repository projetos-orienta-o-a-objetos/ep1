# Instruções de Execução do Programa
Para executar o programa abra o terminal na pasta raiz do projeto e digite este comando para compilar


      $ make 

E para executar

      make run

No Incio sera mostrado o menu inicial do projeto contendo 4 possiveis opcoes:
         
    1- MODO VENDA - responsavel por todo o processo da venda do produto.
    
    2- MODO RECOMENDAÇAO - responsavel por recomendar produtos escolhidos para determinado cliente  
    
    3- MODO ESTOQUE - responsavel por toda a parte de adição de novos produtos e e de suas quantidades

    4- SAIR - que finaliza o programa

em seguinda é pedido para digitar o numero correspondente da opcao

## MODO VENDA
- Ao entrar no modo venda é pedido o numero do cpf da pessoa para verificação de cadastro. Se for cadastrado se iniciara o processo da compra dos produtos. Se nao for, sera iniciado o processo de cadastro e logo em seguida o de venda

- Na parte da venda é listado os produtos existentes e seus preços. é pedido o numero do produto e a quantidade desejada

- Para finalizar a compra e necessario digitar o numero 0

- É mostrado um relatorio da venda e é perguntado se deseja finalizar a compra. Se for confirmado é enviado uma msg de Confirmação, os produtos comprados são diminuidos no estoque e volta para o Menu Inicial. Caso contrario a compra é cancelada e volta para o Menu Inicial

## MODO RECOMENDACAO
- Ao entrar no modo recomendacao é pedido o CPF do cliente

-Se o CPF fo cadastrado é mostrado uma lista de produtos recomendados para aquele cliente Na segunte ordem

     Mais recomendados primeiro e menos recomendados por ultimo
     
     Produtos com mesma preferencia de recomendacao sera recomendados em ordem alfabetica

Pode ser recomendado até 10 produtos, podendo ser menos.

## MODO ESTOQUE

-Ao entrar no modo Estoque um outro menu é mostrado 

      1- Visualizar estoque
      2- Adicionar nova categoria
      3- Adicionar novo produto
      4- Aumentar quantindade de um produto
      5- Sair

### Visualiza Estoque
- Mostra todas as Categorias de produtos em Estoque e seus resptivos produtos
- Caso nao haja nenhuma categoria e mostrado uma msg dizendo que não ha produtos

### Adicionada nova categoria
- Pede para diitar o nome da nova categoria

### Adiciona novo produto 
- Pede para digitar o numero da categoria 
- Nome do produto 
- Preco do produto
- E quantidade inicial do produto

### Aumentar quantindade de um produto
- Pede para digitar o numero da categoria 
- Numero do produto 
- E a quantidade que deseja aumentar

### Sair
-Sai do modo Estoque

# Lista de Dependencias
## Compilador do C++
É necessario ter o compilador de C++ instalado. Para instalar no sistema operacional Linux basta seguir esses passos:

Abra o terminal com o comando: 

      CTR+ALT+T

Digite este comando no Terminal

      $ sudo apt-get install g++

A senha de usuario será pedida. Digite a senha e o  compilador do c++ será instalado

## Pacote Makefile
É necessario ter o software que execute o arquivo Makefile responsavel por compilar todo o projeto instalado na sua maquina.Para instalar basta seguir esses passos.

Abra o terminal

Digite este comando no terminal:
        
    $ sudo apt install make
   
