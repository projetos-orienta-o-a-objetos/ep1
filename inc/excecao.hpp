#ifndef EXCECAO_HPP
#define EXCECAO_HPP

#include <iostream>
#include <exception>

using namespace std;

class Excecao : public exception {
   private:
      Excecao();
      const char* mensagem;
   public:
      Excecao(const char *mensagem):mensagem(mensagem){};
      const char* what();

};

#endif 
