#ifndef CATEGORIA_HPP
#define CATEGORIA_HPP
#include "produto.hpp"
#include <iostream>
#include <vector>

using namespace std;
class Produto;

class Categoria{
   private: 
      string nome_categoria = "Categoria Genérica";
      int quantidade_produtos = 0;
      
   public:
      vector<Produto> produtos;

      Categoria();

      Categoria(string nome_categoria);
      
      Categoria(string nome_categoria, int quantidade_produtos);

      ~Categoria();

      string get_nome_categoria();

      void set_nome_categoria(string nome_categoria);

      int get_quantidade_produtos();

      void set_quantidade_produtos(int quantidade);

      void aumenta_quantidade_produtos(int quantidade);
      
};

#endif