#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <vector>
#include "categoria.hpp"

using namespace std;
class Categoria;
class Produto{
   private:
      string nome= "Produto Genérico";
      float preco= 0.1;
      int quantidade=0;
      

   public:
      vector<string> categorias;

      Produto();
      Produto(string nome, float preco, int quantidade, vector<string>categorias);
      Produto(string nome, float preco, int quantidade); // Esse construtor nao é necessaŕio mas esta dando erro. Verificar depois
      Produto(string nome, float preco);
      ~Produto();

      // Metos gets e sets
      void set_nome(string nome);
      string get_nome();
      void set_preco(float preco);
      float get_preco();
      void set_quantidade(int quantidade);
      int get_quantidade();
      void aumenta_quantidade(int quantidade);
      void diminui_quantidade(int quantidade);
      
};

#endif