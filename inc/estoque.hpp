#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include "categoria.hpp"
#include"produto.hpp"
#include<vector>

using namespace std; 
class Categoria; 
class Produto;

class Estoque {

   public:
      vector<Categoria>produtos_por_categoria;
      Estoque();
      Estoque(vector <Categoria> produtos_por_categoria);
      ~Estoque();

      void set_produtos_por_categoria(vector <Categoria> produtos_por_categoria);
      void set_produtos_por_categoria(Categoria produtos_por_categoria);
      void listar_categorias_produtos();
      void lista_categorias();  
      vector<Produto>  lista_produtos();
      bool diminui_quantidade_estoque(vector<Produto> produtos_cliente);
      void aumenta_quantidade_produto();
      void adiciona_nova_categoria();
      void adiciona_produto();
};

#endif