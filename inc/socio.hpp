#ifndef SOCIO_HPP
#define SOCIO_HPP
#include "cliente.hpp"

using namespace std; 

class Socio : public Cliente {
   private:
      string vencimento_anuidade;
   
   public:
      Socio();
      Socio(string tipo_cliente, string nome, string cpf, string telefone, string endereco, string vencimeto_anuidade);
      Socio(string tipo_cliente, string cpf);
      ~Socio();
      
      string get_vencimeto_anuidade();
      void set_vencimeto_anuidade(string vencimeto_anuidade);
      void imprime_relatorio_compra(vector<Produto> produtos);
};

#endif