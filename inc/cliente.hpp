#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include "produto.hpp"
#include <vector>

using namespace std;

class Cliente{
   private:
      string tipo_cliente;
      string nome= "Generico";
      string cpf="000.000.000-00";
      string telefone="Sem Telefone";
      string endereco= "Sem Endereco";
      vector <Produto> produtos;
      vector<Categoria> categorias;
      
   public:
      Cliente();
      Cliente(string tipo_cliente, string nome, string cpf, string telefone, string endereco);
      Cliente(string tipo_cliente, string cpf);
      ~Cliente();

      string get_tipo_cliente();
      void set_tipo_cliente(string tipo_cliente);

      string get_nome();
      void set_nome(string nome);

      string get_telefone();
      void set_telefone(string telefone);

      string get_enderco();
      void set_endereco(string endereco);

      string get_cpf();
      void set_cpf(string cpf);

      vector<Produto> get_produtos();
      void set_produto(Produto produto);

      vector<Categoria> get_categorias();
      void set_categoria(Categoria categoria);

      virtual void imprime_relatorio_compra(vector<Produto> produtos);
      void adciona_categorias_adiquiridas_pelo_cliente(vector<Produto> produtos);

};

#endif