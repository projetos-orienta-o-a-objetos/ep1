#ifndef MODOSO_HPP
#define MODOS_HPP
#include<string>
#include<vector>
class Categoria;
   using namespace std;
   class Produto;
   void modo_estoque();
   void adiciona_nova_categoria();
   void adiciona_produto();

   void modo_venda();
   void cadastra_cliente(string endereco_arquivo, string cpf);
   void fazer_venda(string cpf);
  
   void modo_recomendacao();
   vector<string>  nivel3(vector<Categoria>categorias_cliente);
   vector<string>  nivel4(vector<Categoria>categorias_cliente);
   vector<string>  nivel2(vector<Categoria>categorias_cliente);
   vector<string>  nivel1(vector<Categoria>categorias_cliente);
   vector<Categoria> ordena_categorias(vector<Categoria>categorias);
   vector<string> ordena_alfabeticamente(vector<string>produtos);
   void LimpaBuufer();

#endif