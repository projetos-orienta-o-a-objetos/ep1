#include <iostream>
#include<fstream>
#include<string>

#include "estoque.hpp"

using namespace std;

Estoque::Estoque(){
   string nome_categoria;
   ifstream arquivo_categorias;
   arquivo_categorias.open("doc//Categorias//1-Lista de Categorias.txt");
   if(arquivo_categorias.is_open()){
      while (getline(arquivo_categorias, nome_categoria)){
         Categoria categoria = {nome_categoria};
         ifstream nome_produtos;
         nome_produtos.open("doc//Categorias//"+nome_categoria+".txt");
         if (nome_produtos.is_open()){
            string linha_nome_produto;
            while(getline(nome_produtos, linha_nome_produto)){
               ifstream info_produtos;
               info_produtos.open("doc//Produtos//"+linha_nome_produto+".txt");
               if(info_produtos.is_open()){
                  string info_produto;
                  string nome_produto;
                  float preco;
                  int quantidade;
                  getline(info_produtos,info_produto);
                  nome_produto =  info_produto;
                  getline(info_produtos, info_produto);
                  preco = stof(info_produto);
                  getline(info_produtos, info_produto);
                  quantidade = stoi(info_produto);
                  Produto produto= {linha_nome_produto, preco, quantidade};
                  while (getline(info_produtos, info_produto)){
                     produto.categorias.push_back(info_produto);
                  }
                  categoria.produtos.push_back(produto);
               }
               info_produtos.close();
            }
         }
         nome_produtos.close();
         produtos_por_categoria.push_back(categoria);  
      }
   }
   arquivo_categorias.close();
}

Estoque::~Estoque(){}

void Estoque::set_produtos_por_categoria(vector <Categoria> produtos_por_categoria){
   unsigned int i;
   for(i=0; i < produtos_por_categoria.size(); i++){
      this->produtos_por_categoria.push_back(produtos_por_categoria[i]);
   }
}

void Estoque::listar_categorias_produtos(){
   /* 
   lista todos as categorias existente no vetor produtos_por_categoria,
   na seguinte ordem: nome da categoria e logo apos o nome de cada produto
   dentro do vetor de categoria
   */
   int i=1;
   for(Categoria categoria_estoque : produtos_por_categoria){
      cout<<"CATEGORIA "<<i<<": "<<categoria_estoque.get_nome_categoria()<<endl;
       
      cout<<"Produtos/   Preco/  Quantidade"<<endl;
       int j=1;
      for(Produto produto_categoria : categoria_estoque.produtos){
         cout<<j<<"- "<< produto_categoria.get_nome()<<"    "<<produto_categoria.get_preco()<<"    "<<produto_categoria.get_quantidade()<<endl;
         j++;
      }
      cout<<endl;
      i++;
   }
}

void Estoque::lista_categorias(){
   int i, quantidade_categorias;
   quantidade_categorias = produtos_por_categoria.size();
   cout<<"CATEGORIAS:"<<endl;
   for(i=0; i< quantidade_categorias;i++){
      cout<<i+1<<"-"<<produtos_por_categoria[i].get_nome_categoria()<<endl;
   }
      cout<<endl;
}

vector<Produto> Estoque::lista_produtos(){
   vector<Produto> produtos;
   int i=0;
   cout<<"Nº /Produtos   /   Preco "<<endl;
   for(Categoria categoria: produtos_por_categoria){
      for(Produto produto : categoria.produtos){
            i++;
            cout<<i<<"- "<< produto.get_nome()<<"    "<<produto.get_preco()<<"R$"<<"    "<<endl;
            produtos.push_back(produto);
      }
   }
   return produtos;
}    

bool Estoque::diminui_quantidade_estoque(vector<Produto> produtos_cliente){
   vector<Produto> produtos_atualizados;
   Produto produto_atualizado;
   string le_arquivo;
   ifstream arquivo_produtos_entrada;
   for(Produto produto : produtos_cliente){
      
      arquivo_produtos_entrada.open("doc//Produtos//"+produto.get_nome()+".txt");
      string nome_produto;
      vector<string>categorias;
      float preco;
      int quantidade;
      if(arquivo_produtos_entrada.is_open()){
         getline(arquivo_produtos_entrada, le_arquivo);
         nome_produto = le_arquivo;
         getline(arquivo_produtos_entrada, le_arquivo );
         preco = stof(le_arquivo);
         getline(arquivo_produtos_entrada, le_arquivo );
         quantidade = stoi(le_arquivo);
         while (getline(arquivo_produtos_entrada, le_arquivo ))
         {  
            categorias.push_back(le_arquivo);
         }
         produto_atualizado= {nome_produto, preco, quantidade, categorias};
         produto_atualizado.diminui_quantidade(produto.get_quantidade());
        
         if(produto_atualizado.get_quantidade()<0){
            cout<<"Não há o produto "+produto_atualizado.get_nome()+" em estoque"<<endl;
            cout<<"Cancelando compra."<<endl;
            return false;
         }
         else{
            produtos_atualizados.push_back(produto_atualizado);
         }
      }
      arquivo_produtos_entrada.close();
   }  

   for(Produto produto : produtos_atualizados){
      
      ofstream arquivo_produtos_saida;
      arquivo_produtos_saida.open("doc//Produtos//"+produto.get_nome()+".txt");
      arquivo_produtos_saida<<produto.get_nome()<<endl;
      arquivo_produtos_saida<<produto.get_preco()<<endl;
      arquivo_produtos_saida<<produto.get_quantidade()<<endl;
      for(string categoria : produto.categorias){
         arquivo_produtos_saida<< categoria<<endl;
      }
      
      arquivo_produtos_saida.close();
      arquivo_produtos_entrada.open("doc//Produtos//"+produto.get_nome()+".txt");
      if(arquivo_produtos_entrada.is_open()){
        
         string nome_produto, nome_categoria;
         int quantidade;
         getline(arquivo_produtos_entrada, le_arquivo);
         nome_produto = le_arquivo;
         getline(arquivo_produtos_entrada, le_arquivo);// pega o preço e descarta
         getline(arquivo_produtos_entrada, le_arquivo);
        
         quantidade = stoi(le_arquivo);
         getline(arquivo_produtos_entrada, le_arquivo);
         nome_categoria = le_arquivo;
         unsigned int i, j;
         for(i=0; i<produtos_por_categoria.size(); i++){
            if (produtos_por_categoria[i].get_nome_categoria()== nome_categoria){
               for(j=0; j<produtos_por_categoria[i].produtos.size(); j++){
                  if(produtos_por_categoria[i].produtos[j].get_nome()==nome_produto){
                     produtos_por_categoria[i].produtos[j].set_quantidade(quantidade);
                  }
               }
            }
         }
       } 
       arquivo_produtos_entrada.close();    
   }

         return true;

}

void Estoque::aumenta_quantidade_produto(){
   system("tput reset");
   if(produtos_por_categoria.size()==0){
      cout<<"Produto há produtos nessa categoria"<<endl;
      cout<<"Volte ao menu e adicione uma categoria para registrar os produtos"<<endl;
   }
   else{
      unsigned int numero_categoria, numero_produto;
      int aumento_da_quantidade=0;
      string nome_produto;
      listar_categorias_produtos();
      cout<<"Digite o numero da categoria correspondente ao produto que deseja aumentar"<<endl;
      cin>>numero_categoria;
      while(numero_categoria > produtos_por_categoria.size() || numero_categoria<1 ){
         cout<<"Digite um número valido"<<endl;
         cin>>numero_categoria;
      }
      if(produtos_por_categoria[numero_categoria-1].produtos.size()==0){
         system("tput reset");
         cout<<"Categoria inexistente"<<endl;
         cout<<"Volte ao menu e adicione um produto. DEpois retorne e aumente sua quantidade"<<endl;
      }
      else{
         cout<<"Digite o numero do produto que deseja aumentar"<<endl;
         cin>>numero_produto;
         while(numero_produto > produtos_por_categoria[numero_categoria-1].produtos.size() || numero_produto<1){
            cout<<"Digite um número valido"<<endl;
            cin>>numero_produto;
         }
         cout<<"Digite a quantidade que deseja aumentar"<<endl;
         cin>> aumento_da_quantidade;
         while (aumento_da_quantidade < 1){
            cout<<"Digite um valor valido"<<endl;
            cin>>aumento_da_quantidade; 
         }
         produtos_por_categoria[numero_categoria-1].produtos[numero_produto-1].aumenta_quantidade(aumento_da_quantidade);
         
         string nome_produto = produtos_por_categoria[numero_categoria-1].produtos[numero_produto-1].get_nome();
         float preco_produto = produtos_por_categoria[numero_categoria-1].produtos[numero_produto-1].get_preco();
         int quantidade = produtos_por_categoria[numero_categoria-1].produtos[numero_produto-1].get_quantidade();

         ofstream arquivo_produto;
         arquivo_produto.open("doc//Produtos//"+nome_produto+".txt");

         arquivo_produto<<nome_produto<<endl;
         arquivo_produto<<preco_produto<<endl;
         arquivo_produto<<quantidade<<endl;
         arquivo_produto<< produtos_por_categoria[numero_categoria-1].get_nome_categoria()<<endl;
 
         arquivo_produto.close();
         system("tput reset");
         cout<<"Quantidade Adicionada com Sucesso!!"<<endl;
      }
   }
}

void Estoque::adiciona_nova_categoria(){
   system("tput reset");
   // Adiciona uma nova categoria setando obrigariamente o nome dela
   string nome_categoria;
   cout<<"Digite o nome da Categoria:"<<endl;
   getchar(); // limpa o buffer
   getline(cin, nome_categoria); 

   Categoria nova_categoria(nome_categoria);
   produtos_por_categoria.push_back(nova_categoria);
   
   ofstream arquivos_categorias;
   arquivos_categorias.open("doc//Categorias//"+nome_categoria+".txt");
   arquivos_categorias.close();

   ofstream lista_categorias;
   lista_categorias.open("doc//Categorias//1-Lista de Categorias.txt", ios::app);
   lista_categorias<<nome_categoria<<endl;
   lista_categorias.close();

   system("tput reset");
   cout<<"Categoria adicionada com sucesso"<<endl;
}

void Estoque::adiciona_produto(){
   system("tput reset");
   if(produtos_por_categoria.size()==0){
      cout<<"Categoria inexistente"<<endl;
      cout<<"Volte ao menu e adicione uma categoria para registrar os produtos"<<endl;
   }
   else{
      unsigned int numero_categoria;
      string nome_produto;
      float preco_produto;
      int quantidade;
      lista_categorias();
      cout<<"Digite o numero da categoria para adicionar o produto"<<endl;
      cin>>numero_categoria;
      while(numero_categoria > produtos_por_categoria.size() || numero_categoria<1 ){
         cout<<"Digite um número valido"<<endl;
         cin>>numero_categoria;
      }

      cout<<"Digite o nome do produto"<<endl;
      getchar(); // limpa o buffer
      getline(cin, nome_produto);
      cout<<"Digite o preco do produto"<<endl;
      cin>> preco_produto;
      while (preco_produto<=0.0){
         cout<<"Digite um número valido"<<endl;
         cin>> preco_produto;
      }

      cout<<"Digite uma quantidade inicial"<<endl;
      cin>> quantidade;
      while (quantidade<0.0){
         cout<<"Digite um número valido"<<endl;
         cin>> quantidade;
      }

      
      Produto novo_produto(nome_produto, preco_produto, quantidade);
      novo_produto.categorias.push_back(produtos_por_categoria[numero_categoria-1].get_nome_categoria());
      produtos_por_categoria[numero_categoria-1].produtos.push_back(novo_produto);
      string nome_categoria = produtos_por_categoria[numero_categoria-1].get_nome_categoria();
      
      ofstream arquivo_categoria;
      arquivo_categoria.open("doc//Categorias//"+nome_categoria+".txt", ios::app);
      arquivo_categoria<<nome_produto<<endl;
      arquivo_categoria.close();
      
      ofstream arquivo_produto;
      arquivo_produto.open("doc//Produtos//"+nome_produto+".txt");
      arquivo_produto<<nome_produto<<endl<<preco_produto<<endl<<quantidade<<endl;
      for(string nome_categoria : novo_produto.categorias){
         arquivo_produto<<nome_categoria;
      }
      arquivo_produto.close();
      
      system("tput reset");
      cout<<"Produto Adicionado com sucesso"<<endl;
   }
}
