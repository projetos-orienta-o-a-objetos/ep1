#include "cliente.hpp"
#include<iostream>
#include<fstream>

Cliente::Cliente(){}

Cliente::Cliente(string tipo_cliente, string nome, string cpf, string telefone, string endereco){
   cout<<"\nATENCAO ENTROU NO CONSTRUTOR DO Cliente"<<endl;
   set_tipo_cliente(tipo_cliente);
   set_nome(nome);
   set_cpf(cpf);
   set_telefone(telefone);
   set_endereco(endereco);
}

Cliente::Cliente(string tipo_cliente, string cpf){
   string info_cliente, tipo, nome, endereco, telefone;
   ifstream arquivo_cliente;
   arquivo_cliente.open("doc//Cadastros//"+cpf+".txt");
   getline(arquivo_cliente,info_cliente); // pega o tipo de cliente e descarta por ja recebe-lo como parametro

   getline(arquivo_cliente,info_cliente);
   nome = info_cliente;

   getline(arquivo_cliente,info_cliente); // pega o cpf e descarta por ja recebe-lo como parametro

   getline(arquivo_cliente,info_cliente);
   endereco = info_cliente;

   getline(arquivo_cliente,info_cliente);
   endereco = info_cliente;

   set_tipo_cliente(tipo_cliente);
   set_nome(nome);
   set_cpf(cpf);
   set_telefone(telefone);
   set_endereco(endereco);

   arquivo_cliente.close();
}

Cliente::~Cliente(){}

string Cliente::get_tipo_cliente(){
   return tipo_cliente;
}
void Cliente ::set_tipo_cliente(string tipo_cliente){
   this->tipo_cliente = tipo_cliente;
}

string Cliente::get_nome(){
   return nome;
}

void Cliente:: set_nome(string nome){
   this->nome = nome;
}

string Cliente:: get_telefone(){
   return telefone;
}

void Cliente::set_telefone(string telefone){
   this->telefone = telefone;
}

string Cliente::get_enderco(){
   return endereco;
}

void Cliente::set_endereco(string endereco){
   this->endereco = endereco;
}

string Cliente::get_cpf(){  
   return cpf;
}

void Cliente::set_cpf(string cpf){
   this->cpf = cpf;
}

vector<Produto> Cliente:: get_produtos(){
   return produtos;
}

void Cliente::set_produto(Produto produto){
   this->produtos.push_back(produto);
}

vector<Categoria> Cliente::get_categorias(){
   return categorias;
}

void Cliente::set_categoria(Categoria categoria){
   this->categorias.push_back(categoria);
}

void Cliente::imprime_relatorio_compra(vector<Produto> produtos){
   system("tput reset");
   cout<<"Produto / "<<"Preco / "<<"Quantidade / "<<"Preco Total"<<endl;
   float total_compra=0;
   for(Produto produto : produtos){
      float total_produto = produto.get_preco()*produto.get_quantidade();
      cout<<produto.get_nome()<<"    "<<produto.get_preco()<<"R$"<<"    "<<produto.get_quantidade()<<"    "<<total_produto<<"R$"<<endl;
      total_compra+=total_produto;
   }
   cout<<"Total a Pagar: "<<total_compra<<"R$"<<endl;
}

void Cliente::adciona_categorias_adiquiridas_pelo_cliente(vector<Produto> produtos){
   vector<Categoria> categorias;
   string linha_informacao;
   for(Produto produto : produtos){
      ifstream informacoes_produtos;
      informacoes_produtos.open("doc//Produtos//"+produto.get_nome()+".txt");
      if(informacoes_produtos.is_open()){
         getline(informacoes_produtos, linha_informacao);
         getline(informacoes_produtos, linha_informacao);
         getline(informacoes_produtos, linha_informacao);
         while (getline(informacoes_produtos, linha_informacao)){  
            int verifica_existencia_categoria=0;
            if(categorias.empty()){
               Categoria categoria = {linha_informacao, produto.get_quantidade()};
               categorias.push_back(categoria);
            }
            else{
               for(Categoria categoria : categorias){
                  if(categoria.get_nome_categoria()== linha_informacao){
                     categoria.aumenta_quantidade_produtos(produto.get_quantidade());
                     verifica_existencia_categoria+=1;
                  } 
               }
               if(verifica_existencia_categoria==0){
                  Categoria categoria = {linha_informacao, produto.get_quantidade()};
                  categorias.push_back(categoria);
               }
            }
         }

      }
   informacoes_produtos.close();
   } 
   string le_arquivo_cliente_existente;
   ifstream arquivo_cliente_existente;
   arquivo_cliente_existente.open("doc//Categorias.Clientes//"+get_cpf()+"-ctgs.txt") ;
   if(arquivo_cliente_existente.is_open()){
      while (getline(arquivo_cliente_existente, le_arquivo_cliente_existente)){
         int verifica_categoria_existe=0;
         int i=0, endereco_vetor;
         for(Categoria categoria : categorias){
            if(categoria.get_nome_categoria()== le_arquivo_cliente_existente){
               verifica_categoria_existe+=1;
               endereco_vetor=i;
            }
           i++; 
         }
         if(verifica_categoria_existe>0){
            //Pega a quantidade daquela categorias existente no arquivo e soma com a que ja existe
            int quantidade_arquivo =0;
            getline(arquivo_cliente_existente, le_arquivo_cliente_existente);
            quantidade_arquivo = stoi(le_arquivo_cliente_existente);
            categorias[endereco_vetor].aumenta_quantidade_produtos(quantidade_arquivo);
            
         }
         else{
            Categoria categoria_arquivo;
            string nome_categoria = le_arquivo_cliente_existente;
            int quantidade;
            getline(arquivo_cliente_existente, le_arquivo_cliente_existente);//Pega a quantidade da categoria e ignora 
            quantidade = stoi(le_arquivo_cliente_existente);
            categoria_arquivo = {nome_categoria, quantidade};
            categorias.push_back(categoria_arquivo);
         }
      } 
      arquivo_cliente_existente.close();
      ofstream categorias_cliente;
      categorias_cliente.open("doc//Categorias.Clientes//"+get_cpf()+"-ctgs.txt");
      for(Categoria categoria : categorias){
         categorias_cliente<<categoria.get_nome_categoria()<<endl;
         categorias_cliente<<categoria.get_quantidade_produtos()<<endl;
      }
      categorias_cliente.close();
   }
   else{
      ofstream categorias_cliente;
      categorias_cliente.open("doc//Categorias.Clientes//"+get_cpf()+"-ctgs.txt");
      for(Categoria categoria : categorias){
         categorias_cliente<<categoria.get_nome_categoria()<<endl;
         categorias_cliente<<categoria.get_quantidade_produtos()<<endl;
      }
      categorias_cliente.close();
   }

}

