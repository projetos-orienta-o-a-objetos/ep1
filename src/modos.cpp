#include"modos.hpp"
#include<iostream>
#include<string>
#include"estoque.hpp"
#include"categoria.hpp"
#include "cliente.hpp"
#include"socio.hpp"
#include"produto.hpp"
#include<fstream>
#include<string>
#include<cstdlib>
#include<ctime>
#include<stdio.h>
#include"excecao.hpp"

const Excecao resposta_errada("Erro: Sua resposta e invalida!");
using namespace std;
Estoque estoque_loja;

void cadastra_cliente(string endereco_arquivo);

void modo_estoque(){
   LimpaBuufer();
   int opcao;
   bool condicao_saida =false;
   system("tput reset");
   cout<<"----Bem Vindo ao modo estoque!!!----"<<endl;

   while (!condicao_saida){
      cout<<"+---Escolha uma das opções abaixo----------+"<<endl;
      cout<<"|---1- Visualizar estoque------------------|"<<endl;
      cout<<"|---2- Adicionar nova categoria------------|"<<endl;
      cout<<"|---3- Adicionar novo produto--------------|"<<endl;
      cout<<"|---4- Aumentar quantindade de um produto--|"<<endl;
      cout<<"|---5- Sair--------------------------------|"<<endl;
      //getchar();
      cin>>opcao;
      while (opcao < 1 || opcao >5){
         cout<<"-----Digite uma Opcao valida-----"<<endl;
         cin>>opcao;
      }
      
      switch (opcao)
      {
         case 1:
            if(estoque_loja.produtos_por_categoria.size()==0){
               system("tput reset");
               cout<<"Não ha produtos em estoque estoque"<<endl;
            }
            else{
               system("tput reset");
               estoque_loja.listar_categorias_produtos();
            }

            break;

         case 2:
            estoque_loja.adiciona_nova_categoria();
            break;

         case 3:
           estoque_loja.adiciona_produto();
            break;

         case 4:
            estoque_loja.aumenta_quantidade_produto();
            break;

          default:
            condicao_saida = true;
            system("tput reset");
            break;
      }
   }

}

void modo_venda(){
   try{
   system("tput reset");
   string  cpf, endereco;
   cout<<"Digite o CPF do cliente sem pontos, espacoes ou tracos"<<endl;
   cin>>cpf;
   system("tput reset");
   endereco = "doc//Cadastros//"+cpf+".txt";
   char cadastrar=' ';
   ifstream arquivo;
   arquivo.open(endereco);
   if(!arquivo.is_open()){
      cout<<"Cliente nao cadastrado. Deseja fazer o cadastro?  [1-SIM]; [0-NAO]"<<endl;
         cin>> cadastrar;

      if (cadastrar=='1'){
         cadastra_cliente(endereco, cpf);
         fazer_venda(cpf);
      }
      else if(cadastrar == '0'){
         LimpaBuufer();
         system("tput reset");
         cout<<"Voltando ao menu inicial"<<endl;
         return;
      }
      else{
         system("tput reset");
         LimpaBuufer();
         throw resposta_errada; 
      }
   }
   else{
      fazer_venda(cpf);
   }
   arquivo.close();
   }
   catch(Excecao& e){
    cout<<e.what()<<endl;
    cout<<"tecle Enter qualquer tecla para iniciar novamente"<<endl;
    LimpaBuufer();
    getchar();
    modo_venda();
   }
}

void LimpaBuufer(){
   char c = ' ';
   while(c!='\n' && c!=EOF){
       c=getchar();
   }
}
void cadastra_cliente(string endereco_arquivo, string cpf){
      LimpaBuufer();
      string nome, telefone, endereco;
   try{
      char ser_socio;
      system("tput reset");
      cout<<"Digite o nome completo do cliente"<<endl;
      getline(cin, nome); 
      system("tput reset");

      cout<<"Digite o numero do celular."<< endl <<"Formato Exemplo : (99)99999-9999"<<endl;
      getline(cin, telefone); 

      system("tput reset");
      cout<<"Digite o endereço."<<endl<<"O endereço é pedido para fazer entregas caso haja necessidade"<<endl; 
      getline(cin, endereco);

      system("tput reset");
      cout<<"Deseja ser sócio??[1-SIM] [0-NAO]"<<endl;
      cout<<"ATENCAO: Clientes Associados possuem 15% de desconto em todas as compras,"<<endl<<"sendo necessário apenas pagar uma taxa anual de 200 reais!!!"<<endl;
      cin>>ser_socio;

      if(ser_socio=='1'){
         LimpaBuufer();
         string vencimento_anuidade, tipo_cliente = "Associado";
         system("tput reset");
         cout<<"Digite numero correspondente do mês que deseja para o vencimento da anuidade"<<endl;
         cin>> vencimento_anuidade;
         Socio novo_cliente={tipo_cliente, nome, cpf, telefone, endereco, vencimento_anuidade};
         ofstream cadastro_arquivo;
         cadastro_arquivo.open(endereco_arquivo, ios::app);
         cadastro_arquivo<< tipo_cliente<<endl;
         cadastro_arquivo<< nome<<endl;
         cadastro_arquivo<<cpf<<endl;
         cadastro_arquivo<<telefone<<endl;
         cadastro_arquivo<<endereco<<endl;
         cadastro_arquivo<<vencimento_anuidade<<endl;
         cadastro_arquivo.close();
         system("tput reset");
         cout<<"Cliente Cadastrado com Sucesso!"<<endl;
         cout<<"Digite qualquer tecla para continuar"<<endl;
         LimpaBuufer();
      }
      else if (ser_socio=='0'){  
         LimpaBuufer();
         string tipo_cliente = "Nao Associado";
         Cliente novo_cliente={tipo_cliente, nome, cpf, telefone, endereco};
         ofstream cadastro_arquivo;
         cadastro_arquivo.open(endereco_arquivo, ios::app);
         cadastro_arquivo<< tipo_cliente<<endl;
         cadastro_arquivo<< nome<<endl;
         cadastro_arquivo<<cpf<<endl;
         cadastro_arquivo<<telefone<<endl;
         cadastro_arquivo<<endereco<<endl; 
         cadastro_arquivo.close();
         system("tput reset");
         cout<<"Cliente Cadastrado com Sucesso!"<<endl;
         cout<<"Digite qualquer tecla para continuar"<<endl;
      }
      else{
         system("tput reset");
         throw resposta_errada; 
      }
      
   } 
   catch(Excecao& e){
      cout<<e.what()<<endl;
      cout<<"tecle qualquer tecla para iniciar novamente"<<endl;
      LimpaBuufer();
      cadastra_cliente(endereco_arquivo, cpf);
   }

}

void fazer_venda(string cpf){
   LimpaBuufer();
   ifstream arquivo_cliente;
   try{
   system("tput reset");
   string info_cliente;
   string tipo_cliente;
   arquivo_cliente.open("doc//Cadastros//"+cpf+".txt");
   if(arquivo_cliente.is_open()){
      getline(arquivo_cliente,info_cliente);
      tipo_cliente = info_cliente;
      
      if(tipo_cliente == "Associado"){
         Socio socio = {tipo_cliente, cpf};
         cout<<"Cliente "<<socio.get_nome()<<endl<<"Digite qualquer tecla para listar os produtos da loja"<<endl;
         LimpaBuufer();
         vector<Produto> produtos = estoque_loja.lista_produtos(), produtos_cliente;
         bool finalizar_compra=false;
         int index;

         while (!finalizar_compra){
         system("tput reset");
        estoque_loja.lista_produtos();
         cout<<"Digite os numeros dos produtos que o cliente deseja comprar e suas respectivas quantidades."<<endl;
         cout<<"Digite 0(zero) para finalizar a compra."<<endl;
            //LimpaBuufer();
            cout<<"Digite o numero do produto: ";
            cin>>index;
            unsigned int i = index;
            cout<<endl;
            LimpaBuufer();
            if(index==0){
               cout<<"Entrou na condicao de saida"<<endl;
               finalizar_compra=true;
            }
            else if(index<0 || i >produtos.size()){
                cout<<"Numero de produto invalido. Aperte qualquer tecla para tentar novamente"<<endl;
                getchar();
                LimpaBuufer();
             }
            else if(isalpha(index)){
                cout<<"Nao consegui tratar o erro corretamente "<<isalpha(index)<<endl;
                getchar();
                throw resposta_errada;
             }
             
            else{
               int quantidade=0;
               cout<<"Digite a quantidade: ";
               cin>>quantidade;
               cout<<endl;
               produtos[index-1].set_quantidade(quantidade);
               produtos_cliente.push_back(produtos[index-1]);
            }
         }
         socio.imprime_relatorio_compra(produtos_cliente);
         cout<<"Confirmar compra?[1- Sim | 0-Nao]"<<endl;
         int confirmar;
         cin>>confirmar;
         if(confirmar){
            bool compra_realizada = estoque_loja.diminui_quantidade_estoque(produtos_cliente);

            if (compra_realizada){
               socio.adciona_categorias_adiquiridas_pelo_cliente(produtos_cliente);
               cout<<"Compra Realizada com Sucesso!"<<endl<<"Voltando ao menu inicial"<<endl;
         } }
      }

      else{
         Cliente cliente = {tipo_cliente, cpf};
         cout<<"Cliente "<<cliente.get_nome()<<endl<<"Digite qualquer tecla para listar os produtos da loja"<<endl;
         LimpaBuufer();
         vector<Produto> produtos = estoque_loja.lista_produtos(), produtos_cliente;
         bool finalizar_compra=false;
         int index;

         while (!finalizar_compra){
         system("tput reset");
        estoque_loja.lista_produtos();
         cout<<"Digite os numeros dos produtos que o cliente deseja comprar e suas respectivas quantidades."<<endl;
         cout<<"Digite 0(zero) para finalizar a compra."<<endl;
            //LimpaBuufer();
            cout<<"Digite o numero do produto: ";
            cin>>index;
            unsigned int i=index;
            cout<<endl;
            LimpaBuufer();
            if(index==0){
               cout<<"Entrou na condicao de saida"<<endl;
               finalizar_compra=true;
            }
            else if(index<0 || i >produtos.size()){
                cout<<"Numero de produto invalido. Aperte qualquer tecla para tentar novamente"<<endl;
                getchar();
                LimpaBuufer();
             }
            else if(isalpha(index)){
                cout<<"Nao consegui tratar o erro corretamente "<<isalpha(index)<<endl;
                getchar();
                throw resposta_errada;
             }
             
            else{
               int quantidade=0;
               cout<<"Digite a quantidade: ";
               cin>>quantidade;
               cout<<endl;
               produtos[index-1].set_quantidade(quantidade);
               produtos_cliente.push_back(produtos[index-1]);
            }
         }
         cliente.imprime_relatorio_compra(produtos_cliente);
         cout<<"Confirmar compra?[1- Sim | 0-Nao]"<<endl;
         int confirmar;
         cin>>confirmar;
         if(confirmar){
            estoque_loja.diminui_quantidade_estoque(produtos_cliente);
            bool compra_realizada = estoque_loja.diminui_quantidade_estoque(produtos_cliente);
            if (compra_realizada)
               cliente.adciona_categorias_adiquiridas_pelo_cliente(produtos_cliente);
         } 
      }
   }
   else cout<<"Erro na leitura do arquivo"<<endl;

   arquivo_cliente.close();
   }
   catch(Excecao& e){
      cout<<e.what()<<endl;
      cout<<"tecle qualquer tecla para iniciar novamente"<<endl;
      getchar();
      arquivo_cliente.close();
      fazer_venda(cpf);
      
   }
   
}

void modo_recomendacao(){
   system("tput reset");
   string cpf;
   LimpaBuufer();
   cout<<"Digite o CPF do cliente sem pontos, espacoes ou tracos"<<endl;
   getline(cin, cpf);
   ifstream arquivo_categorias_do_cliente;
   arquivo_categorias_do_cliente.open("doc//Categorias.Clientes//"+cpf+"-ctgs.txt");
   if(arquivo_categorias_do_cliente.is_open()){
      vector<Categoria>categorias_cliente;
      string linha,nome_categoria;
      vector<string> produtos_recomendar;
      int quantidade;
      while (getline(arquivo_categorias_do_cliente, linha)){
         nome_categoria = linha;
         getline(arquivo_categorias_do_cliente, linha);
         quantidade = stoi(linha);
         Categoria categoria = {nome_categoria, quantidade};
         categorias_cliente.push_back(categoria);
      }
      categorias_cliente = ordena_categorias(categorias_cliente);
   
      int nivel = categorias_cliente.size();

      switch (nivel){
      case 0:
         break;

      case 1:
         produtos_recomendar=nivel1(categorias_cliente);
         break;
      
      case 2:
         produtos_recomendar=nivel2(categorias_cliente);
         break;
      
      case 3:
         
         produtos_recomendar=nivel3(categorias_cliente);
         break;

      default:
         
         produtos_recomendar= nivel4(categorias_cliente);
         break;
      }
      system("tput reset");
      cout<<"PRODUTOS RECOMENDADOS PARA O CLIENTE: "<<endl;
      for(string produto : produtos_recomendar){
         cout<<produto<<endl;
      }
      cout<<endl;
      cout<<"Esses produtos podem ser encontrados no momento da venda"<<endl;
      cout<<"Digite qualquer letra para voltar ao menu inicial"<<endl;
      LimpaBuufer();
      arquivo_categorias_do_cliente.close();
      system("tput reset");
      
   }
   else {
      cout<<"ERRO - CLIENTE NAO CADASTRADO"<<endl<<"Voltando ao Menu Inicial"<<endl;
      arquivo_categorias_do_cliente.close();
      return;}
}
   
vector<string> nivel4(vector<Categoria>categorias_cliente){
   
   vector<string> nomes_categorias;
   for(int i=0; i<4;i++){
      nomes_categorias.push_back(categorias_cliente[i].get_nome_categoria());
   }

   vector<Categoria>categorias_recomendar;
   for(int i=0; i<4;i++){
      for(Categoria categoria : estoque_loja.produtos_por_categoria){
         if(nomes_categorias[i]==categoria.get_nome_categoria()){
            categorias_recomendar.push_back(categoria);
            break;
         }
      }
   }

      
   vector<string> todos_produtos_recomendar;
   unsigned int j=4;
   for(Categoria categoria : categorias_recomendar){
   vector<string> produtos_recomendar_categoria;
      for(Produto produto : categoria.produtos){
         produtos_recomendar_categoria.push_back(produto.get_nome());
      }
      produtos_recomendar_categoria= ordena_alfabeticamente(produtos_recomendar_categoria);
      for(unsigned int i = 0; i<j && i<produtos_recomendar_categoria.size(); i++){
         todos_produtos_recomendar.push_back(produtos_recomendar_categoria[i]);
      }
      j--;
   }
   return todos_produtos_recomendar;
   
}

vector<string>  nivel3(vector<Categoria>categorias_cliente){
   vector<string> nomes_categorias;
   for(int i=0; i<3;i++){
      nomes_categorias.push_back(categorias_cliente[i].get_nome_categoria());
   }

   vector<Categoria>categorias_recomendar;
   for(int i=0; i<3;i++){
      for(Categoria categoria : estoque_loja.produtos_por_categoria){
         if(nomes_categorias[i]==categoria.get_nome_categoria()){
            categorias_recomendar.push_back(categoria);
            break;
         }
      }
   }

      
   vector<string> todos_produtos_recomendar;
   unsigned int j=5, k=2;
   for(Categoria categoria : categorias_recomendar){
   vector<string> produtos_recomendar_categoria;
      for(Produto produto : categoria.produtos){
         produtos_recomendar_categoria.push_back(produto.get_nome());
      }
      produtos_recomendar_categoria= ordena_alfabeticamente(produtos_recomendar_categoria);
      for(unsigned int i = 0; i<j && i<produtos_recomendar_categoria.size(); i++){
         todos_produtos_recomendar.push_back(produtos_recomendar_categoria[i]);
      }
      j-=k;
      k--;
   }
   return todos_produtos_recomendar;
   
}

vector<string>  nivel2(vector<Categoria>categorias_cliente){
    cout<<"Entrou no 2"<<endl;
    
   vector<string> nomes_categorias;
   for(int i=0; i<2;i++){
      nomes_categorias.push_back(categorias_cliente[i].get_nome_categoria());
   }

   vector<Categoria>categorias_recomendar;
   for(int i=0; i<2;i++){
      for(Categoria categoria : estoque_loja.produtos_por_categoria){
         if(nomes_categorias[i]==categoria.get_nome_categoria()){
            categorias_recomendar.push_back(categoria);
            break;
         }
      }
   }

      
   vector<string> todos_produtos_recomendar;
   unsigned int j=6, k=2;
   for(Categoria categoria : categorias_recomendar){
   vector<string> produtos_recomendar_categoria;
      for(Produto produto : categoria.produtos){
         produtos_recomendar_categoria.push_back(produto.get_nome());
      }
      produtos_recomendar_categoria= ordena_alfabeticamente(produtos_recomendar_categoria);
      for(unsigned int i = 0; i<j && i<produtos_recomendar_categoria.size(); i++){
         todos_produtos_recomendar.push_back(produtos_recomendar_categoria[i]);
      }
      j-=k;
      k--;
   }
   return todos_produtos_recomendar;
   
}

vector<string>  nivel1(vector<Categoria>categorias_cliente){
   vector<string> nomes_categorias;
   for(int i=0; i<1;i++){
      nomes_categorias.push_back(categorias_cliente[i].get_nome_categoria());
   }

   vector<Categoria>categorias_recomendar;
   for(int i=0; i<1;i++){
      for(Categoria categoria : estoque_loja.produtos_por_categoria){
         if(nomes_categorias[i]==categoria.get_nome_categoria()){
            categorias_recomendar.push_back(categoria);
            break;
         }
      }
   }

      
   vector<string> todos_produtos_recomendar;
   unsigned int j=10; 
   for(Categoria categoria : categorias_recomendar){
   vector<string> produtos_recomendar_categoria;
      for(Produto produto : categoria.produtos){
         produtos_recomendar_categoria.push_back(produto.get_nome());
      }
      produtos_recomendar_categoria= ordena_alfabeticamente(produtos_recomendar_categoria);
      for(unsigned int i = 0; i<j && i<produtos_recomendar_categoria.size(); i++){
         todos_produtos_recomendar.push_back(produtos_recomendar_categoria[i]);
      }
      j--;
   }
   return todos_produtos_recomendar;
   
}

vector<Categoria> ordena_categorias(vector<Categoria> categorias_cliente){
   Categoria aux; 
   for(unsigned int i = 0; i<categorias_cliente.size()-1;i++){
      for(unsigned int j=i+1; j<categorias_cliente.size();j++){
         if(categorias_cliente[j].get_quantidade_produtos()>categorias_cliente[i].get_quantidade_produtos()){
            aux = categorias_cliente[j];
            categorias_cliente[j] = categorias_cliente[i];
            categorias_cliente[i]= aux;
            
         }
      }
   }
  
   return categorias_cliente;
}

vector<string> ordena_alfabeticamente(vector<string>produtos){
   for(string produto : produtos){
      if(produto[0]>=97 && produto[0]<=122){
            produto[0]-=32; 
         }
      }
   
   string aux;
   int r;
   for(unsigned int i=0; i<produtos.size()-1;i++){
      for(unsigned int j=i+1; j<produtos.size();j++){
         r = produtos[i].compare(produtos[j]);
         if(r>0){
            aux = produtos[i];
            produtos[i]=produtos[j];
            produtos[j]= aux;
         }
      }
   }
 return produtos;
}

