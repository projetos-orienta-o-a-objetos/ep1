#include <iostream>
#include "estoque.hpp"
#include "categoria.hpp"
#include "produto.hpp"
#include"modos.hpp"
#include <vector>
#include "excecao.hpp"
#include <cctype>

const Excecao resposta_errada("Erro: Sua resposta e invalida!");
int main(){
   system("tput reset");
   int escolha;
   bool condicao_saida = true;
   cout<< endl;
   cout<< "---Gerenciador de Vendas e Estoque---" <<endl;

   while (condicao_saida){

      cout<< endl;
      cout<< "-----------------" << endl;
      cout<< "| 1- Modo VENDA |" << endl;
      cout<< "-----------------" << endl;
      cout<< "------------------------" << endl;
      cout<< "| 2- Modo RECOMENDACAO |" << endl;
      cout<< "------------------------" << endl;
      cout<< "-------------------" << endl;
      cout<< "| 3- Modo ESTOQUE |" << endl;
      cout<< "-------------------" << endl;
      cout<< "-----------" << endl;
      cout<< "| 4- SAIR |" << endl;
      cout<< "-----------" << endl;
      cout<< endl;
      cout<< "Escolha uma das opcoes acima digitando seu respectivo número" << endl;
      cout<<endl;

      try{
        cin>>escolha;
        if(escolha ==0 || escolha>4 || isalpha(escolha)){
            throw resposta_errada;
        }
      }
      catch(Excecao& e){
         cout<<e.what()<<endl;
         return 0;
      }
      
      switch (escolha){
         case 1:
            modo_venda();
            break;   
         case 2:
            modo_recomendacao();
            break;
         case 3:
            modo_estoque();
            break;
         case 4:
         cout<<"Ate mais...!"<<endl;
            condicao_saida = false;
            break;
         default:
            break;
         }   
   }
   return 0;
}
