#include "produto.hpp"
#include <iostream>

Produto::Produto(){}
Produto::Produto(string nome, float preco, int quantidade, vector<string>categorias){
   this->nome= nome;
   this->preco= preco;
   this->quantidade = quantidade;
   this-> categorias = categorias;
}
Produto::Produto(string nome, float preco, int quantidade){
   this->nome= nome;
   this->preco= preco;
   this->quantidade = quantidade;
}

Produto::Produto(string nome, float preco){
   set_nome(nome);
   set_preco(preco);
}

Produto::~Produto(){}

void Produto:: set_nome(string nome){
   this->nome = nome;
}

string Produto::get_nome(){
   return nome;
}

void Produto::set_preco(float preco){
   this->preco = preco;
}

float Produto::get_preco(){
   return preco;
}


void Produto::set_quantidade(int quantidade){
   this->quantidade = quantidade;
}


int Produto::get_quantidade(){
   return quantidade;

}

void Produto::aumenta_quantidade(int quantidade){
   this->quantidade += quantidade;
}

void Produto::diminui_quantidade(int quantidade_cliente){
   this->quantidade -= quantidade_cliente;

}