#include "socio.hpp"
#include<iostream>
#include<fstream>

using namespace std;

Socio::Socio(){}
Socio::Socio(string tipo_cliente, string nome, string cpf, string telefone, string endereco, string vencimento_anuidade){
   Cliente(tipo_cliente, nome,cpf,telefone, endereco);
   set_vencimeto_anuidade(vencimento_anuidade);
   
}
Socio::Socio(string tipo_cliente, string cpf){
   string info_cliente, tipo, nome, endereco, telefone, vencimento_anuidade;
   ifstream arquivo_cliente;
   arquivo_cliente.open("doc//Cadastros//"+cpf+".txt");
   getline(arquivo_cliente,info_cliente); // pega o tipo de cliente e descarta por ja recebe-lo como parametro

   getline(arquivo_cliente,info_cliente);
   nome = info_cliente;

   getline(arquivo_cliente,info_cliente); // pega o cpf e descarta por ja recebe-lo como parametro

   getline(arquivo_cliente,info_cliente);
   endereco = info_cliente;

   getline(arquivo_cliente,info_cliente);
   endereco = info_cliente;

   getline(arquivo_cliente,info_cliente);
   vencimento_anuidade= info_cliente;

   //Cliente(tipo_cliente, nome, cpf, telefone, endereco);
   set_tipo_cliente(tipo_cliente);
   set_nome(nome);
   set_cpf(cpf);
   set_telefone(telefone);
   set_endereco(endereco);
   set_vencimeto_anuidade(vencimento_anuidade);

   arquivo_cliente.close();

      
}
Socio::~Socio(){}

string Socio::get_vencimeto_anuidade(){
   return vencimento_anuidade;
}

void Socio::set_vencimeto_anuidade(string vencimento_anuidade){
   this->vencimento_anuidade = vencimento_anuidade;
}

void Socio::imprime_relatorio_compra(vector<Produto> produtos){
   system("tput reset");
   cout<<"Produto / "<<"Preco / "<<"Quantidade / "<<"Preco Total"<<endl;
   float total_compra=0;
   for(Produto produto : produtos){
      float total_produto = produto.get_preco()*produto.get_quantidade();
      cout<<produto.get_nome()<<"    "<<produto.get_preco()<<"R$"<<"    "<<produto.get_quantidade()<<"    "<<total_produto<<"R$"<<endl;
      total_compra+=total_produto;
   }
   float desconto = total_compra*0.15;
   cout<<"Desconto de 15% para Associado: "<<desconto<<"R$"<<endl;
   cout<<"Total a Pagar: "<<total_compra-desconto<<"R$"<<endl;
}



