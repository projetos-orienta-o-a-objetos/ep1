#include "categoria.hpp"
#include <iostream>

using namespace std;
Categoria::Categoria(){}

Categoria::Categoria(string nome_categoria){
   this-> nome_categoria = nome_categoria;
}

Categoria::Categoria(string nome_categoria, int quantidade_produtos){
   set_nome_categoria(nome_categoria);
   set_quantidade_produtos(quantidade_produtos);
}
Categoria::~Categoria(){}

string Categoria::get_nome_categoria(){
   return nome_categoria;
}

void Categoria::set_nome_categoria(string nome_categoria){
   this->nome_categoria = nome_categoria;
}

int Categoria::get_quantidade_produtos(){
   return quantidade_produtos;
}

void Categoria::set_quantidade_produtos(int quantidade){
   quantidade_produtos = quantidade;
}

void Categoria::aumenta_quantidade_produtos(int quantidade){
   quantidade_produtos+= quantidade;
}
